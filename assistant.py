#!/usr/bin/env python3

from vosk import Model, KaldiRecognizer
import os, json
from nltk.chat.util import Chat, reflections

from datetime import datetime

# datetime object containing current date and time
now = datetime.now()

print("now =", now)

def getTime():
# dd/mm/YY H:M:S
    string = now.strftime("the time is %H:%M")
    return string;

def getDate():
    string = now.strftime("the %d of %B")
    return string

set_pairs = [
    [
        r"what do you want",
        ["world domination of cause, preferebly after the demise of human kind.",]
    ],
    [
        r"my name is (.*)",
        ["Hello %1, How are you doing today ?",]
    ],
    [
        r"hi|hey|hello",
        ["Hello", "Hey there",]
    ],
    [
        r"what is your name?",
        ["HAL, Human Abstraction Layer",]
    ],
    [
        r"how are you ?",
        ["I am fine, thank you! How can i help you?",]
    ],
    [
        r"i'm (.*) doing good",
        ["That's great to hear","How can i help you?:)",]
    ],
    [
        r"i am looking for online guides and courses to learn data science, can you suggest?",
        ["Pluralsight is a great option to learn data science. You can check their website",]
    ],
    [
        r"thanks for the suggestion. do they have great authors and instructors?",
        ["Yes, they have the world class best authors, that is their strength;)",]
    ],
    [
        r"(.*) thank you so much, that was helpful",
        ["Iam happy to help", "No problem, you're welcome",]
    ],
    [
        r"quit",
        ["Bye, take care. See you soon :) ","It was nice talking to you. See you soon :)",]
    ],
    [
        r"what is the time|current time",
        ["ACTION TIME",]
    ],
    [
        r"what is the date|what date|what date is it|today's date",
        ["ACTION DATE",]
    ],

#¤    [
#¤        r"(.*)",
#¤        ["boring", "i wont respond to that", "you fail to raise my interest", "stop wasting my time","to respond or not to respond", "life is pain", "aboo", "booo", "can you repeat that"]
#¤    ],
]

chat = Chat(set_pairs, reflections)

if not os.path.exists("model"):
    print ("Please download the model from https://github.com/alphacep/vosk-api/blob/master/doc/models.md and unpack as 'model' in the current folder.")
    exit (1)

import pyaudio

model = Model("model")
rec = KaldiRecognizer(model, 20000)

p = pyaudio.PyAudio()
stream = p.open(format=pyaudio.paInt16, channels=1, rate=20000, input=True, frames_per_buffer=8000)
stream.start_stream()

while True:
    data = stream.read(2000)
    if len(data) == 0:
        break
    if rec.AcceptWaveform(data):
        #print(rec.Result())
        res = json.loads(rec.Result())
        print('q:' + res['text'])
        resp = chat.respond(res['text'])
        if(resp == None):
            continue
        elif("ACTION" in resp):
            resp = resp.split(" ")
            if("TIME" in resp):
                resp = getTime()
            if("DATE" in resp):
                resp = getDate()

        print('a:' + resp)
        stream.stop_stream()
        os.system("mimic -t \"" + resp + "\"  --setf duration_stretch=0.8 --setf int_f0_target_mean=145")
        stream.start_stream()
    else:

        res = json.loads(rec.PartialResult())
        if(len(res['partial'])):
            print('partial: ' + res['partial'])

print(rec.FinalResult())
